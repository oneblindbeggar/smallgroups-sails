var request = require('supertest');

describe('AuthController', function() {

  describe('login()', function() {
    it('should redirect to /', function (done) {
      request(sails.hooks.http.app)
        .post('/auth/login')
        .send({ email: 'test@test.com', password: 'test123' })
        .expect(302)
        .expect('location','/', done);
    });
  });

});